package com.devcamp.s50.task56.rest_api.model;

import java.time.LocalDate;

public class CDrink {
    private int id;
    private String maNuocUong;
    private String tenNuocUong;
    private long price;
    private LocalDate ngayTao;
    private LocalDate ngayCapNhat;

    /**
     * @param id
     * @param maNuocUong
     * @param tenNuocUong
     * @param price
     * @param ngayTao
     * @param ngayCapNhat
     */

    public CDrink(int id, String maNuocUong, String tenNuocUong, long price, LocalDate ngayTao, LocalDate ngayCapNhat) {
        super();
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong; 
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public void setId (int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getMaNuocUong() {
        return this.maNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public String getTenNuocUong() {
        return this.tenNuocUong;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPrice() {
        return this.price;
    }

    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }

    public LocalDate getNgayTao() {
        return this.ngayTao;
    }

    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public LocalDate getNgayCapNhat() {
        return this.ngayCapNhat;
    }
}

