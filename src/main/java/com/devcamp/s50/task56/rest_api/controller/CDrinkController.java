package com.devcamp.s50.task56.rest_api.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56.rest_api.model.CDrink;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/devcamp-drinks")
    public ArrayList<CDrink> getDrinkList() {
        ArrayList<CDrink> drinkList = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        CDrink traTac = new CDrink(1,"TRATAC", "Trà tắc", 10000, today, today);
        CDrink coca = new CDrink(2,"COCA","Cocacola",15000, today, today);
        CDrink pepsi = new CDrink(3,"PEPSI","Pepsi",15000, today, today);
        drinkList.add(traTac);
        drinkList.add(coca);
        drinkList.add(pepsi);
        return drinkList;
    }
}
